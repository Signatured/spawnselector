package me.signatured.spawnselector;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

public class TownsManager {
	
private static TownsManager am = new TownsManager();
	
	public static TownsManager getManager() {
		return am;
	}
	
	public BanditTown getBanditTown(String name) {
		for (BanditTown t : BanditTown.banditTowns) {
			if (t.getName().equals(name)) {
				return t;
			}
		}
		return null;
	}
	
	public SheriffTown getSheriffTown(String name) {
		for (SheriffTown t : SheriffTown.sheriffTowns) {
			if (t.getName().equals(name)) {
				return t;
			}
		}
		return null;
	}
	
	public void loadTowns() {
		FileConfiguration fc = Main.getPlugin().getConfig();
		ConfigurationSection bt = fc.getConfigurationSection("bandit towns");
		
		if (fc != null && bt != null) {
			for (String name : bt.getKeys(false)) {
				String path = "bandit towns." + name + ".";
				
				Location loc = deserializeLoc(Main.getPlugin().getConfig().getString(path + "location"));
				
				new BanditTown(name, loc, loc.getPitch(), loc.getYaw());
			}
		}
		
		ConfigurationSection st = fc.getConfigurationSection("sheriff towns");
		
		if (fc != null && st != null) {
			for (String name : st.getKeys(false)) {
				String path = "bandit towns." + name + ".";
				
				Location loc = deserializeLoc(Main.getPlugin().getConfig().getString(path + "location"));
				
				new SheriffTown(name, loc, loc.getPitch(), loc.getYaw());
			}
		}
	}
	
	public void createBanditTown(String name, Location loc) {
		new BanditTown(name, loc, loc.getPitch(), loc.getYaw());
		
		FileConfiguration fc = Main.getPlugin().getConfig();
		 
		fc.set("bandit towns." + name, null);
		String path = "bandit towns." + name + ".";
		
		String townLoc = serializeLoc(loc);
		
		fc.set(path + "location", townLoc);
		Main.getPlugin().saveConfig();
	}
	
	public void createSheriffTown(String name, Location loc) {
		new SheriffTown(name, loc, loc.getPitch(), loc.getYaw());
		
		FileConfiguration fc = Main.getPlugin().getConfig();
		 
		fc.set("sheriff towns." + name, null);
		String path = "sheriff towns." + name + ".";
		
		String townLoc = serializeLoc(loc);
		
		fc.set(path + "location", townLoc);
		Main.getPlugin().saveConfig();
	}
	
	public String serializeLoc(Location l) {
		return l.getWorld().getName() + "," + l.getBlockX() + "," + l.getBlockY() + "," + l.getBlockZ() + "," + l.getPitch() + "," + l.getYaw();
	}
	
	public Location deserializeLoc(String s) {
		String[] st = s.split(",");
		Location loc = new Location(Bukkit.getWorld(st[0]), Integer.parseInt(st[1]), Integer.parseInt(st[2]), Integer.parseInt(st[3]));
		
		loc.setPitch(Float.parseFloat(st[4]));
		loc.setYaw(Float.parseFloat(st[5]));
		return loc;
	}
}
