package me.signatured.spawnselector;

import java.util.ArrayList;

import org.bukkit.Location;

public class BanditTown {
	
	public static ArrayList<BanditTown> banditTowns = new ArrayList<BanditTown>();
	
	private String name;
	private Location loc;
	
	public BanditTown(String name, Location loc, float pitch, float yaw) {
		this.name = name;
		this.loc = loc;
		this.loc.setPitch(pitch);
		this.loc.setYaw(yaw);
		
		banditTowns.add(this);
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Location getLocation() {
		return this.loc;
	}
	
	public void setLocation(Location loc, float pitch, float yaw) {
		this.loc = loc;
		this.loc.setPitch(pitch);
		this.loc.setPitch(yaw);
	}
}
