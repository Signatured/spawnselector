package me.signatured.spawnselector;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	
	private static Main instance;
	
	public Main() {
		instance = this;
	}
	
	public static Main getPlugin() {
		return instance;
	}
	
	public void onEnable() {
		saveConfig();
		
		new TownsManager();
		new SheriffSelector();
		new BanditSelector();
		
		TownsManager.getManager().loadTowns();
		
		Bukkit.getServer().getPluginManager().registerEvents(new Listeners(), this);
		
		getCommand("setbandittown").setExecutor(new CommandManager());
		getCommand("setsherifftown").setExecutor(new CommandManager());
	}
}
