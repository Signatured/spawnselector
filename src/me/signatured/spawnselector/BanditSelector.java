package me.signatured.spawnselector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Wool;

public class BanditSelector {
	
	private static BanditSelector instance;
	
	public static BanditSelector getInstance() {
		return instance;
	}
	
	public ArrayList<String> players = new ArrayList<String>();
	
	public Inventory inv;
	
	private ItemStack town1, town2, town3, town4, town5;
	private ItemStack home, ganghq;
	
	public BanditSelector() {
		instance = this;
		inv = Bukkit.getServer().createInventory(null, 18, "Choose a bandit town!");
		
		town1 = createTown(DyeColor.GREEN, ChatColor.translateAlternateColorCodes('&', "&bTown1"));
		town2 = createTown(DyeColor.YELLOW, ChatColor.translateAlternateColorCodes('&', "&bTown2"));
		town3 = createTown(DyeColor.BLUE, ChatColor.translateAlternateColorCodes('&', "&bTown3"));
		town4 = createTown(DyeColor.RED, ChatColor.translateAlternateColorCodes('&', "&bTown4"));
		town5 = createTown(DyeColor.PURPLE, ChatColor.translateAlternateColorCodes('&', "&bTown5"));
		
		home = createHome(new ItemStack(Material.WOOD_DOOR, 1), ChatColor.translateAlternateColorCodes('&', "&6Private Home"), 
				Arrays.asList(ChatColor.translateAlternateColorCodes('&', "&5Must own a"), ChatColor.translateAlternateColorCodes('&', "&5private house!")));
		ganghq = createHome(new ItemStack(Material.IRON_DOOR, 1), ChatColor.translateAlternateColorCodes('&', "&6Gang HQ"), 
				Arrays.asList(ChatColor.translateAlternateColorCodes('&', "&5Gang must own"), ChatColor.translateAlternateColorCodes('&', "&5an HQ!")));
		
		inv.setItem(2, town1);
		inv.setItem(3, town2);
		inv.setItem(4, town3);
		inv.setItem(5, town4);
		inv.setItem(6, town5);
		
		inv.setItem(12, home);
		inv.setItem(14, ganghq);
	}
	
	private ItemStack createTown(DyeColor dc, String name) {
		ItemStack i = new Wool(dc).toItemStack(1);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		i.setItemMeta(im);
		return i;
	}
	
	private ItemStack createHome(ItemStack item, String name, List<String> lore) {
		ItemStack i = item;
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		im.setLore(lore);
		i.setItemMeta(im);
		return i;
	}
	
	public void show(Player p) {
		p.openInventory(inv);
		p.setHealth(20D);
		
		if (!players.contains(p.getName())) players.add(p.getName());
	}
}
