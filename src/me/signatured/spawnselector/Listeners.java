package me.signatured.spawnselector;

import me.signatured.wildwest.util.PrisonHandler;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;

public class Listeners implements Listener {
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		
		if (e.getInventory().getName().equalsIgnoreCase(BanditSelector.getInstance().inv.getName()) || e.getInventory().getName().equalsIgnoreCase(SheriffSelector.getInstance().inv.getName())) {
			if (BanditSelector.getInstance().players.contains(player.getName())) {
				Bukkit.getServer().getScheduler().runTaskLater(Main.getPlugin(), new Runnable() {
					@Override
					public void run() {
						BanditSelector.getInstance().show(player);
					}
				}, 2);
			} else if (SheriffSelector.getInstance().players.contains(player.getName())) {
				SheriffSelector.getInstance().show(player);
			}
		}
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		Player player = (Player) e.getWhoClicked();
		
		if (e.getInventory().getName().equalsIgnoreCase(BanditSelector.getInstance().inv.getName())) {
			ItemStack item = e.getCurrentItem();
			
			if (item != null && item.hasItemMeta()){
				switch (item.getItemMeta().getDisplayName()) {
				
				case "�bTown1":
					if (BanditSelector.getInstance().players.contains(player.getName())) BanditSelector.getInstance().players.remove(player.getName());
					if (PrisonHandler.prison.contains(player.getName())) PrisonHandler.prison.remove(player.getName());
					
					player.teleport(TownsManager.getManager().getBanditTown("1").getLocation());
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6You chose Town1!"));
					player.closeInventory();
					break;
					
				case "�bTown2":
					if (BanditSelector.getInstance().players.contains(player.getName())) BanditSelector.getInstance().players.remove(player.getName());
					if (PrisonHandler.prison.contains(player.getName())) PrisonHandler.prison.remove(player.getName());
					
					player.teleport(TownsManager.getManager().getBanditTown("2").getLocation());
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6You chose Town2!"));
					player.closeInventory();
					break;
					
				case "�bTown3":
					if (BanditSelector.getInstance().players.contains(player.getName())) BanditSelector.getInstance().players.remove(player.getName());
					if (PrisonHandler.prison.contains(player.getName())) PrisonHandler.prison.remove(player.getName());
					
					player.teleport(TownsManager.getManager().getBanditTown("3").getLocation());
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6You chose Town3!"));
					player.closeInventory();
					break;
					
				case "�bTown4":
					if (BanditSelector.getInstance().players.contains(player.getName())) BanditSelector.getInstance().players.remove(player.getName());
					if (PrisonHandler.prison.contains(player.getName())) PrisonHandler.prison.remove(player.getName());
					
					player.teleport(TownsManager.getManager().getBanditTown("4").getLocation());
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6You chose Town4!"));
					player.closeInventory();
					break;
					
				case "�bTown5":
					if (BanditSelector.getInstance().players.contains(player.getName())) BanditSelector.getInstance().players.remove(player.getName());
					if (PrisonHandler.prison.contains(player.getName())) PrisonHandler.prison.remove(player.getName());
					
					player.teleport(TownsManager.getManager().getBanditTown("5").getLocation());
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6You chose Town5!"));
					player.closeInventory();
					break;
					
				case "�6Private Home":
					if (BanditSelector.getInstance().players.contains(player.getName())) BanditSelector.getInstance().players.remove(player.getName());
					if (PrisonHandler.prison.contains(player.getName())) PrisonHandler.prison.remove(player.getName());
					
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6You chose private home!"));
					player.closeInventory();
					break;
					
				case "�6Gang HQ":
					if (BanditSelector.getInstance().players.contains(player.getName())) BanditSelector.getInstance().players.remove(player.getName());
					if (PrisonHandler.prison.contains(player.getName())) PrisonHandler.prison.remove(player.getName());
					
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6You chose gang hq!"));
					player.closeInventory();
					break;
				}
				e.setCancelled(true);
			}
		} else if (e.getInventory() == SheriffSelector.getInstance().inv) {
			ItemStack item = e.getCurrentItem();
			
			if (item != null && item.hasItemMeta()){
				switch (item.getItemMeta().getDisplayName()) {
				
				case "�bTown1":
					SheriffSelector.getInstance().players.remove(player.getName());
					
					player.teleport(TownsManager.getManager().getSheriffTown("1").getLocation());
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6You chose Town1!"));
					player.closeInventory();
					break;
					
				case "�bTown2":
					SheriffSelector.getInstance().players.remove(player.getName());
					
					player.teleport(TownsManager.getManager().getSheriffTown("2").getLocation());
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6You chose Town2!"));
					player.closeInventory();
					break;
					
				case "�bTown3":
					SheriffSelector.getInstance().players.remove(player.getName());
					
					player.teleport(TownsManager.getManager().getSheriffTown("3").getLocation());
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6You chose Town3!"));
					player.closeInventory();
					break;
					
				case "�bTown4":
					SheriffSelector.getInstance().players.remove(player.getName());
					
					player.teleport(TownsManager.getManager().getSheriffTown("4").getLocation());
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6You chose Town4!"));
					player.closeInventory();
					break;
					
				case "�bTown5":
					SheriffSelector.getInstance().players.remove(player.getName());
					
					player.teleport(TownsManager.getManager().getSheriffTown("5").getLocation());
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6You chose Town5!"));
					player.closeInventory();
					break;
					
				case "�6Private Home":
					SheriffSelector.getInstance().players.remove(player.getName());
					
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6You chose private home!"));
					player.closeInventory();
					break;
					
				case "�6Sheriff HQ":
					SheriffSelector.getInstance().players.remove(player.getName());
					
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6You chose sheriff hq!"));
					player.closeInventory();
					break;
				}
				e.setCancelled(true);
			}
		}
	}
}
