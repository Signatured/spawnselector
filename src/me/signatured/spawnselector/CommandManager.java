package me.signatured.spawnselector;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandManager implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("setsherifftown")) {
			if (sender.hasPermission("wildwest.admin")) {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					
					if (args.length == 1) {
						TownsManager.getManager().createSheriffTown(args[0], player.getLocation());
						player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aSheriff town set!"));
					}
				}
			}
		} else if (cmd.getName().equalsIgnoreCase("setbandittown")) {
			if (sender.hasPermission("wildwest.admin")) {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					
					if (args.length == 1) {
						TownsManager.getManager().createBanditTown(args[0], player.getLocation());
						player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aBandit town set!"));
					}
				}
			}
		}
		return true;
	}
}
